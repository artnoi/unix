#!/bin/sh

# Run this script to change substring 'Hack' to 'Inconsolata'.
# If you want to revert, just use 'git restore' on this file

sed 's/Hack/Inconsolata/g' alacritty.toml;
