lib = {}

lib.import_module = function(mod)
	local ok, _ = pcall(require, mod)

	if not ok then
		print("init.lua: failed to load module " .. mod)
	end
end

function lib.table_length(T)
	local count = 0
	for _ in pairs(T) do
		count = count + 1
	end
	return count
end

return lib
