local actions = require("telescope.actions")
local finders = require("telescope.builtin")

require("telescope").setup({
	defaults = {
		prompt_prefix = " ❯ ",
		initial_mode = "insert",
		sorting_strategy = "ascending",
		layout_config = {
			prompt_position = "top",
		},
		mappings = {
			i = {
				["<ESC>"] = actions.close,
				["<C-j>"] = actions.move_selection_next,
				["<C-k>"] = actions.move_selection_previous,
				["<TAB>"] = actions.toggle_selection + actions.move_selection_next,
				["<C-s>"] = actions.send_selected_to_qflist,
				["<C-q>"] = actions.send_to_qflist,
			},
		},
	},

    pickers = {
        -- See help: live_grep
        live_grep = {
            additional_args = function(opts)
                return { "--hidden", "--no-ignore" }
            end
        },
        -- See help: find_files
        find_files = {
            no_ignore = true,
            hidden = true,
        },
    },

	extensions = {
		fzf = {
			fuzzy = true,
			override_generic_sorter = true, -- override the generic sorter
			override_file_sorter = true, -- override the file sorter
			case_mode = "smart_case", -- "smart_case" | "ignore_case" | "respect_case"
		},
	},
})

local Telescope = setmetatable({}, {
	__index = function(_, k)
		if vim.bo.filetype == "NvimTree" then
			vim.cmd.wincmd("l")
		end
		return finders[k]
	end,
})

-- Ctrl-p = fuzzy finder
vim_keymap("n", "<C-P>", function()
	local ok = pcall(Telescope.git_files, { show_untracked = true })
	if not ok then
		Telescope.find_files()
	end
end, { silent = false, noremap = true })
