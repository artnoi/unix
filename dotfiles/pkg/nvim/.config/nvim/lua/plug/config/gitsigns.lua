require("gitsigns").setup({
	signs = {
		add = { text = "+" },
		change = { text = "~" },
		changedelete = { text = "=" },
	},
	on_attach = function(buf)
		local gs = package.loaded.gitsigns
		local opts = { buffer = buf, expr = true, replace_keycodes = false }

		-- Navigation
		vim_keymap("n", "]c", "&diff ? ']c' : '<CMD>Gitsigns next_hunk<CR>'", opts)
		vim_keymap("n", "[c", "&diff ? '[c' : '<CMD>Gitsigns prev_hunk<CR>'", opts)

		-- Actions
		vim_keymap({ "n", "v" }, "<leader>gr", gs.reset_hunk, { buffer = buf })
		vim_keymap({ "n", "v" }, "<leader>ga", gs.stage_hunk, { buffer = buf })
		vim_keymap("n", "<leader>gb", gs.stage_buffer, { buffer = buf })
		vim_keymap("n", "<leader>gp", gs.preview_hunk, { buffer = buf })

		-- Text object
		vim_keymap({ "o", "x" }, "ah", ":<C-U>Gitsigns select_hunk<CR>", { buffer = buf })
	end,
})
