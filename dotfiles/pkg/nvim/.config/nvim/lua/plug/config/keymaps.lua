-- nvim-wide plugin keymaps
-- The keymaps defined here is initialized before the plugins are attached.
-- So, this is usually the keymaps that attaches the plugins.
--
-- Kepmaps that are only enabled when plugin `foo` is attached should be defined in plug.config.foo instead

local nvtree = package.loaded["nvim-tree.api"].tree
vim_keymap("n", "<C-f>", nvtree.toggle, { silent = false, noremap = false })

vim.api.nvim_create_user_command("CD", function(opts)
	nvtree.change_root(opts.args)
end, { nargs = 1 })

local telescope = package.loaded["telescope.builtin"]
vim_keymap("n", "<leader>f", telescope.builtin, { silent = false, noremap = true })
vim_keymap("n", "<leader>h", telescope.help_tags, { silent = false, noremap = true })
vim_keymap("n", "'b", telescope.buffers, { silent = false, noremap = true })
vim_keymap("n", "'f", telescope.find_files, { silent = false, noremap = true })
vim_keymap("n", "'g", telescope.live_grep, { silent = false, noremap = true })
vim_keymap("n", "'gs", telescope.git_status, { silent = false, noremap = true })
