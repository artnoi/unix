-- Load other plugin configs
import_module("plug.config.nvim-tree")
import_module("plug.config.gitsigns")
import_module("plug.config.telescope")
import_module("plug.config.treesitter")
import_module("plug.config.comment")
import_module("plug.config.nvim-surround")
import_module("plug.config.colorscheme")

-- Keymaps for plugins
import_module("plug.config.keymaps")
