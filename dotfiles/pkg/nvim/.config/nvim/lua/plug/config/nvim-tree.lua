-- Filesystem tree explorer
require("nvim-tree").setup({
	sort_by = "case_sensitive",
	view = {
		adaptive_size = true,
		mappings = {
			list = {
				{
					key = "u",
					action = "dir_up",
				},
			},
		},
	},

	diagnostics = {
		enable = true,
		show_on_dirs = true,
		debounce_delay = 150,
		icons = {
			hint = "",
			info = "",
			warning = "",
			error = "",
		},
	},

	renderer = {
		group_empty = true,
	},
	filters = {
		dotfiles = false,
	},
})
