-- Set colorscheme with Vimscript call

-- Use default iceberg as colorscheme
--vim.api.nvim_command("colorscheme iceberg")
-- Use default onedarker as colorscheme
--vim.api.nvim_command('colorscheme onedarker')

-- Use catppuccin
require("plug.config.catppuccin").flavour("macchiato") -- latte, frappe, macchiato, mocha
