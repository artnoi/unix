local U = {}

local fmt_group = vim.api.nvim_create_augroup("FORMATTING", { clear = true })

---Common format-on-save for lsp servers that implements formatting
---@param client table
---@param buf integer
function U.fmt_on_save(client, buf)
	if client.supports_method("textDocument/formatting") then
		vim.api.nvim_create_autocmd("BufWritePre", {
			group = fmt_group,
			buffer = buf,
			callback = function()
				vim.lsp.buf.format({
					timeout_ms = 3000,
					buffer = buf,
				})
			end,
		})
	end
end

---LSP servers capabilities w/ nvim-cmp
function U.capabilities()
	-- The nvim-cmp almost supports LSP's capabilities so You should advertise it to LSP servers..
	local capabilities = vim.lsp.protocol.make_client_capabilities()

	return require("cmp_nvim_lsp").update_capabilities(capabilities)
end

---Disable formatting for servers | Handled by null-ls
---@param client table
---@see https://github.com/jose-elias-alvarez/null-ls.nvim/wiki/Avoiding-LSP-formatting-conflicts
function U.disable_formatting(client)
	client.server_capabilities.documentFormattingProvider = false
	client.server_capabilities.documentRangeFormattingProvider = true
end

---Creates LSP mappings
---@param buf number
function U.keymaps(buf)
	local opts = { buffer = buf }

    -- "go to" stuff
	vim_keymap("n", "gD", vim.lsp.buf.declaration, opts)
	vim_keymap("n", "gd", vim.lsp.buf.definition, opts)
	vim_keymap("n", "gi", vim.lsp.buf.implementation, opts)
	vim_keymap("n", "gr", vim.lsp.buf.references, opts)

	vim_keymap("n", "K", vim.lsp.buf.hover, opts)
	vim_keymap("n", "<C-k>", vim.lsp.buf.signature_help, opts)

	vim_keymap("n", "<space>wa", vim.lsp.buf.add_workspace_folder, opts)
	vim_keymap("n", "<space>wr", vim.lsp.buf.remove_workspace_folder, opts)
	vim_keymap("n", "<space>wl", function()
		print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
	end, opts)
	vim_keymap("n", "<space>rn", ":RenameSaveAll<CR>", opts)
	vim_keymap("n", "<space>ca", vim.lsp.buf.code_action, opts)
	vim_keymap("n", "<space>f", vim.lsp.buf.format, opts)
	vim_keymap("n", "<space>D", vim.lsp.buf.type_definition, opts)

	--vim_keymap("v", "<space>f", vim.lsp.buf.range_formatting, opts)
end

return U
