local lsputils = require("plug.lsp.lsputils")
local server_config = require("plug.lsp.servers.config")

local nls = require("null-ls")
local fmt = nls.builtins.formatting
local dgn = nls.builtins.diagnostics
local cda = nls.builtins.code_actions
local hvr = nls.builtins.hover

-- Configuring null-ls
nls.setup({
	flags = server_config.lspflags,
	sources = {
		fmt.trim_whitespace.with({
			filetypes = { "text", "zsh", "yaml", "toml", "make", "conf", "tmux" },
		}),
		-- NOTE:
		-- 1. both needs to be enabled to so prettier can apply eslint fixes
		-- 2. prettierd should come first to prevent occassional race condition
		fmt.prettierd,
		fmt.eslint_d,
		fmt.stylua,
		fmt.shfmt,
		fmt.rustfmt.with({
			extra_args = function(params)
				local Path = require("plenary.path")
				local cargo_toml = Path:new(params.root .. "/" .. "Cargo.toml")

				if cargo_toml:exists() and cargo_toml:is_file() then
					for _, line in ipairs(cargo_toml:readlines()) do
						local edition = line:match([[^edition%s*=%s*%"(%d+)%"]])
						if edition then
							return { "--edition=" .. edition }
						end
					end
				end

				-- default edition when we don't find `Cargo.toml` or the `edition` in it.
				return { "--edition=2021" }
			end,
		}),

		dgn.golangci_lint.with({
			args = { "run", "--out-format=json", "$DIRNAME", "--path-prefix", "$ROOT", "--fix", "--fast" },
		}),

		dgn.eslint,
		dgn.markdownlint,
		--        dgn.shellcheck,
		--        dgn.luacheck.with({
		--            extra_args = { '--globals', 'vim', '--std', 'luajit' },
		--        }),
		cda.gitsigns,
		--        cda.eslint_d,
		--        cda.shellcheck,

		hvr.dictionary,
	},

	on_attach = function(client, buf)
		lsputils.fmt_on_save(client, buf)
		lsputils.keymaps(buf)
	end,
})
