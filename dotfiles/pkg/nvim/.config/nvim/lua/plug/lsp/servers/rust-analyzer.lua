local lsputils = require("plug.lsp.lsputils")
local server_config = require("plug.lsp.servers.config")

server_config.lspconfig["rust_analyzer"].setup({
	flags = server_config.lspflags,

	on_attach = function(client, bufnr)
		lsputils.keymaps(bufnr)
	end,

	root_dir = server_config.lsputil.root_pattern("Cargo.lock", "Cargo.toml", ".git"),

	settings = {
		["rust-analyzer"] = {
			imports = {
				granularity = {
					group = "module",
				},
				prefix = "self",
			},
			cargo = {
				buildScripts = {
					enable = true,
				},
			},
			procMacro = {
				enable = true,
			},
		},
	},
})
