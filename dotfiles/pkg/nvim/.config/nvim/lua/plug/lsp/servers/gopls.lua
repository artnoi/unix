local lsputils = require("plug.lsp.lsputils")
local server_config = require("plug.lsp.servers.config")

server_config.lspconfig["gopls"].setup({
	flags = server_config.lspflags,
	on_attach = function(client, bufnr)
		lsputils.fmt_on_save(client, bufnr)
		lsputils.keymaps(bufnr)
	end,

	filetypes = { "go", "gomod" },
	root_dir = server_config.lsputil.root_pattern("go.work", "go.mod", ".git"),
})
