local server_config = require("plug.lsp.servers.config")

server_config.lspconfig["efm"].setup({
	cmd = { "efm-langserver", "-q" }, -- Quiet
	flags = server_config.lspflags,

	init_options = {
		documentFormatting = true,
		codeAction = true,
		diagnostics = true,
		hover = true,
	},

	on_attach = function(client, bufnr)
		utils.fmt_on_save(client, bufnr)
		utils.keymaps(bufnr)
	end,

	root_dir = server_config.lsputil.root_pattern(".git"),

	settings = {
		rootMarkers = { ".git", "." },
		languages = {
			go = {
				{
					formatCommand = "gofmt",
					formatStdin = true,
				},
			},
			rust = {
				{
					formatCommand = "rustfmt",
					formatStdin = true,
				},
			},
		},
	},
})
