-- Disable LSP logging
vim.lsp.set_log_level(vim.lsp.log_levels.OFF)

import_module("plug.lsp.completion")
-- Enable separate language servers here
import_module("plug.lsp.servers.null-ls")
import_module("plug.lsp.servers.rust-analyzer")
import_module("plug.lsp.servers.gopls")
