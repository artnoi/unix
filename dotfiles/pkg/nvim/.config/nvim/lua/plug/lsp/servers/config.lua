F = {}

F.lspflags = {
	-- This is the default in Nvim 0.7+
	debounce_text_changes = 150,
}
F.lspconfig = require("lspconfig")
F.lsputil = F.lspconfig.util

return F
