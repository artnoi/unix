# My minimal nvim plugins and config

In my view, the installed plugins can be categorized into 2 main types.

- Language support plugins (e.g. `lspconfig`, `null-ls`, and `nvim-cmp`)

    For language support, plugins can be further divided
    into LSP and non-LSP software.

    As of the writing, **my nvim LSP connects directly
    to the host's language servers**.

    Extra linting and formatting is then injected to nvim with `null-ls.nvim`.

    > The configuration for language plugins support is in `plug/lsp`,
    and individual language server can be configured in `plug/lsp/servers`.

    Language support is only for the language I use **a lot**, e.g. Go and Rust.

- NeoVim UI plugins (e.g. `nvim-tree` and `telescope`)

    These plugins are not for coding per-se, but they still improve our productivity.

    > The configuration for non-language support plugins are in `plug/config`.

    I try to add more of these plugins as I become more familiar with nvim configuration.
