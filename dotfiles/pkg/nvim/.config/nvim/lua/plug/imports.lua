-- plugins.lua defines NeoVim plugins I use.
-- Only Packer.nvim package definitions are declared here,
-- while other personal settings and initialization is done separately in plug.config.
-- The only types of settings allowed here is dependency specification and plugin installation.

-- Automatically run :PackerCompile whenever plugins.lua is updated with an autocommand:
vim.api.nvim_create_autocmd("BufWritePost", {
	group = vim.api.nvim_create_augroup("PACKER", { clear = true }),
	pattern = "imports.lua",
	command = "source <afile> | PackerCompile",
})

return require("packer").startup(function(use)
	-- Packer can manage itself
	use("wbthomason/packer.nvim")
	-- Required by some other plugins
	use("nvim-lua/plenary.nvim")

	-- Colorscheme
	-- Configure the colorscheme in plug.config.colorscheme
	use({
		"cocopon/iceberg.vim",
		event = "BufEnter",
	})
	use({
		"catppuccin/nvim",
		as = "catppuccin",
	})
	use("LunarVim/Onedarker.nvim")

	-- NERDTree replacement in Lua
	use({
		"kyazdani42/nvim-tree.lua",
		requires = "kyazdani42/nvim-web-devicons", -- FS icons
	})

	-- Code comment
	use("numToStr/Comment.nvim")
	-- Surround
	use({
		"kylechui/nvim-surround",
		tag = "*",
	})

	-- Git buffer integration
	use("lewis6991/gitsigns.nvim")
	-- Better diff-view
	use("sindrets/diffview.nvim")

	-- Telescope and FZF
	use({
		{
			"nvim-telescope/telescope.nvim",
		},
		-- This FZF implementation is in C and in source form.
		{
			"nvim-telescope/telescope-fzf-native.nvim",
			after = "telescope.nvim",
			run = "make", -- Compile plug-in after install/update
			config = function()
				require("telescope").load_extension("fzf")
			end,
		},
		{
			"nvim-telescope/telescope-symbols.nvim",
			after = "telescope.nvim",
		},
	})

	-- Tree-Sitter
	use({
		{
			"nvim-treesitter/nvim-treesitter",
			run = ":TSUpdate",
		},
		{ "nvim-treesitter/playground", after = "nvim-treesitter" },
		{ "nvim-treesitter/nvim-treesitter-textobjects", after = "nvim-treesitter" },
		{ "nvim-treesitter/nvim-treesitter-refactor", after = "nvim-treesitter" },
		{ "windwp/nvim-ts-autotag", after = "nvim-treesitter" },
		{ "JoosepAlviste/nvim-ts-context-commentstring", after = "nvim-treesitter" },
	})

	-- Coding: LSP support, linting, and completion
	-- use('mattn/efm-langserver')
	use("neovim/nvim-lspconfig")
	use("jose-elias-alvarez/null-ls.nvim")
	use({
		{
			"hrsh7th/nvim-cmp",
			requires = {
				{
					"L3MON4D3/LuaSnip",
					requires = {
						{
							"rafamadriz/friendly-snippets",
							event = "CursorHold",
						},
					},
				},
			},
		},
		{ "hrsh7th/cmp-nvim-lsp" },
		{ "hrsh7th/cmp-path", after = "nvim-cmp" },
		{ "hrsh7th/cmp-buffer", after = "nvim-cmp" },
		{ "saadparwaiz1/cmp_luasnip", after = "nvim-cmp" },
	})

	use("windwp/nvim-spectre")
end)
