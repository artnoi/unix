local sfind = string.find
local nvimconf_path = home_dir .. "/.config/nvim"
local lua_path = nvimconf_path .. "/lua/"

vim.api.nvim_create_user_command("CheatSheet", function(opts)
	local args = opts.args

	if sfind(args, "treesit") then
		target = "plug/config/treesitter"
	elseif sfind(args, "tele") or sfind(opts.args, "scope") then
		target = "plug/config/telescope"
	elseif sfind(args, "git") then
		target = "plug/config/gitsigns"
	elseif sfind(args, "surr") then
		target = "plug/config/nvim-surround"
	elseif sfind(args, "null") then
		target = "plug/lsp/server/null-ls"

    elseif args == "comment" then
        target = "plug/config/comment"
	elseif args == "plug" then
		target = "plug/config/keymaps"
	elseif args == "lsp" then
		target = "plug/lsp/lsputils"
	elseif args == "." then
		target = "local/cmd"
	else
		target = "local/keymaps"
	end

	vim.cmd("view " .. lua_path .. target .. ".lua")
end, { nargs = "?" })

vim.api.nvim_create_user_command("EditConfig", function(opts)
    vim.cmd("edit" .. nvimconf_path)
end, { nargs = 0 })

vim.api.nvim_create_user_command("RenameSaveAll", function(opts)
    vim.lsp.buf.rename()
    vim.cmd("wa")
    print("RenameSaveAll: saved all buffers")
end, {})
