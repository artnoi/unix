utils = {}

function utils.map(m, k, v, silent, noremap)
	vim.keymap.set(m, k, v, { silent = silent, noremap = noremap })
end

return utils
