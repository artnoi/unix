-- This file only specifies vim/nvim keymaps.
-- For plugin keymaps, see plug.config.keymaps,
-- and for LSP keymaps, see plug.lsp.lsp.

-- Stupid shortcuts for executing Ex and shell commands
vim_keymap("n", ";", ":", { silent = false, noremap = false })
vim_keymap("v", ";", ":", { silent = false, noremap = false })
vim_keymap("n", "'", ":!", { silent = false, noremap = false })
vim_keymap("v", "'", ":!", { silent = false, noremap = false })

-- Frequently used keys and actions
vim_keymap("n", "Y", "y$", { silent = false, noremap = true })
vim_keymap("n", "n", "nzzzv", { silent = false, noremap = true })
vim_keymap("n", "N", "Nzzzv", { silent = false, noremap = true })
vim_keymap("n", "J", "mzJ`z", { silent = false, noremap = true })

-- Paste what you yanked, not what deleted
vim_keymap("n", ",p", '"0p', { silent = false, noremap = false })
vim_keymap("n", ",P", '"0P', { silent = false, noremap = false })

-- Moving text/lines
vim_keymap("n", "<Leader>j :m", ".+1<CR>==", { silent = false, noremap = true })
vim_keymap("n", "<Leader>k :m", ".-2<CR>==", { silent = false, noremap = true })
vim_keymap("i", "<C-j> <esc>:m", ".+1<CR>==", { silent = false, noremap = true })
vim_keymap("i", "<C-k> <esc>:m", ".-2<CR>==", { silent = false, noremap = true })
vim_keymap("v", "<J :m>", "'>+1<CR>gv=gv", { silent = false, noremap = true })
vim_keymap("v", "<K :m>", "'>-2<CR>gv=gv", { silent = false, noremap = true })

-- Arrow keys OR CTRL-VimKeys are used to resize windows
vim_keymap("n", "<up>", ":resize +5<CR>", { silent = false, noremap = false })
vim_keymap("n", "<down>", ":resize -5<CR>", { silent = false, noremap = false })
vim_keymap("n", "<left>", ":vertical resize -3<CR>", { silent = false, noremap = false })
vim_keymap("n", "<right>", ":vertical resize +3<CR>", { silent = false, noremap = false })
vim_keymap("n", "<C-h>", "<C-w>h", { silent = false, noremap = true })
vim_keymap("n", "<C-j>", "<C-w>j", { silent = false, noremap = true })
vim_keymap("n", "<C-k>", "<C-w>k", { silent = false, noremap = true })
vim_keymap("n", "<C-l>", "<C-w>l", { silent = false, noremap = true })

-- Opening new files
vim_keymap("n", "<C-n>", ":tabedit", { silent = false, noremap = false })
vim_keymap("n", "<C-s>", ":tabn<CR>", { silent = false, noremap = false })
vim_keymap("n", "<C-c>", ":w !pbcopy<CR>", { silent = false, noremap = false })

vim_keymap("n", "<space>e", vim.diagnostic.open_float, { silent = true, noremap = true })
vim_keymap("n", "[d", vim.diagnostic.goto_prev, opts)
vim_keymap("n", "]d", vim.diagnostic.goto_next, opts)

-- Undo breakpoints
--vim_keymap("i", ".", ".<C-g>u", { silent = false, noremap = true })
--vim_keymap("i", ",", ",<C-g>u", { silent = false, noremap = true })
--vim_keymap("i", "!", "!<C-g>u", { silent = false, noremap = true })
--vim_keymap("i", "?", "?<C-g>u", { silent = false, noremap = true })

