-- Neovim files
vim.opt.directory = { home_dir .. "/.nvim/.swap//" }
vim.opt.undodir = { home_dir .. "/.nvim/.undodir//" }

-- Floating diagnostic window
vim.diagnostic.config({
	underline = true,
	signs = true,
	float = {
		source = "always",
		border = border,
	},
})

local o = vim.o
local g = vim.g

-- Display
g.nvcode_termcolors = 256
o.termguicolors = true

-- Editor UI
g.mapleader = ","
o.incsearch = true
o.title = true
o.showmode = true
o.mouse = "a"
o.backspace = "indent,eol,start"

o.number = true
o.numberwidth = 5
o.relativenumber = true
o.cursorline = true
o.ruler = true

-- Editing UI
o.expandtab = true
-- o.smarttab = true
o.cindent = true
-- o.autoindent = true
o.wrap = true
o.textwidth = 300
o.tabstop = 4
o.shiftwidth = 0
o.softtabstop = -1 -- If negative, shiftwidth value is used
o.list = true
o.listchars = "trail:·,nbsp:◇,tab:→ ,extends:▸,precedes:◂"
-- o.listchars = 'eol:¬,space:·,lead: ,trail:·,nbsp:◇,tab:→-,extends:▸,precedes:◂,multispace:···⬝,leadmultispace:│   ,'
-- o.formatoptions = 'qrn1'
-- Makes neovim and host OS clipboard play nicely with each other
o.clipboard = "unnamedplus"
-- Case insensitive searching UNLESS /C or capital in search
o.ignorecase = true
o.smartcase = true
-- Better buffer splitting
o.splitright = true
o.splitbelow = true
