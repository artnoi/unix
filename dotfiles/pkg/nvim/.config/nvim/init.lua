-- Import frequently used function here and only once
-- and declare them as global names
lib = require("lib.lib")
import_module = lib.import_module
table_length = lib.table_length

vim_keymap = vim.keymap.set
home_dir = vim.env.XDG_CONFIG_HOME or vim.fn.expand("~")

-- Native nvim config
import_module("local.g_put") -- Overwrite _G.put so we can easily print Lua maps
import_module("local.settings")
import_module("local.cmd")
import_module("local.keymaps")

-- Plugins
import_module("plug.imports") -- Plugin definitions
import_module("plug.config.conf") -- Plugin config

-- LSP config after plugins are loaded
import_module("plug.lsp.lsp")
import_module("local.statusline")
