# If not running interactively, don't do anything.
[[ -z "${PS1}" ]] && return;

echo "bash_profile";

# clear the prompt a bit and work on
typeset -a imports=(
	"${HOME}/.bashrc"
);

[[ -n "${PS1}" ]]\
&& for f in ${imports[@]}; do
	[[ -f "$f" ]] && . "$f";
done;
