# If not running interactively, don't do anything
[ -z "${PS1}" ] && return;

SHELL_CONF="${HOME}/.config/shell";

PROGNAME="zshrc";

. "${SHELL_CONF}/init.sh"
. "${SHELL_CONF}/import/import.zsh" "zshrc"

# No persistent shell history
HISTCONTROL=$HISTCONTROL${HISTCONTROL+:}ignoredups;
HISTCONTROL=ignoreboth;
unset HISTFILE;

# Load machine specific environment
source_shell_script_dir "$HOME/.machine" "zshrc";
