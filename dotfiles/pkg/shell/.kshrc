# If not running interactively, don't do anything
[ -z "${PS1}" ] && return;

echo "[kshrc]";

export SHELL_CONF="${HOME}/.config/shell";

. "$SHELL_CONF/init.sh"
. "$SHELL_CONF/import/import.ksh"

source_shell_script_dir "$HOME/.machine" ".kshrc;
