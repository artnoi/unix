# .os - setup OS variable and shell ENV
PROGNAME="os"

# Tested on bash, mksh, ksh, zsh on GNU/Linux
[ -z $SHELL_CONF ] && SHELL_CONF="${HOME}/.config/shell";

# Avoid redundant sourcing
test "$OS_CACHED" == 1 && return

# Get kernel info
kern=$(uname);
case "${kern}" in

# Get OS info
	'Darwin')
        pront "found macOS";

        export OS="$kern";
        source_shell_script "$SHELL_CONF/mac.sh" "$PROGNAME";
		;;

	'OpenBSD')
        pront "found OpenBSD";

        export OS="$kern";
        source_shell_script "$SHELL_CONF/openbsd.sh" "$PROGNAME";
        ;;

	'FreeBSD')
        pront "found FreeBSD";

        source_shell_script "$SHELL_CONF/freebsd.sh" "$PROGNAME";
        export OS="$kern";
        ;;

	'Linux')
        pront "found Linux"

		[ -n "$(command -v apt-get )" ] && export OS="Debian";
		[ -n "$(command -v pacman )" ] && export OS="Arch";
		[ -n "$(command -v xbps)" ] && export OS="Void";
		[ -n "$(command -v dnf)" ] && export OS="Redhat";

        source_shell_script "$SHELL_CONF/linux.sh" "$PROGNAME";
		;;

    *)
        pront "unknown OS"
        ;;
esac;
export OS_CACHED=1;
