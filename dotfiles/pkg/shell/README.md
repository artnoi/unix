# Config for shell

This directory contains shell environment and its setup scripts,
and it mirrors the home directory if installed by the installer script.

## Entrypoints

Each shell will have its own entrypoint(s), usually an rc file,
e.g. `.bashrc` for Bash, `.kshrc` for Korn Shell, etc.

These entrypoints must set `SHELL_CONF` to `$HOME/.config/shell`,
and then perform some shell-specific setups, like command history,
etc.

After setting up, these entrypoints will chain-import other
default scripts via their own import scripts,
e.g. [`import/import.zsh` for ZShell](./import/import.zsh),
or [`import/import.bash` for Bash](./import/import.bash).

After the default import scripts return, the entrypoints will
load user/machine-specfic environments in [`$HOME/.machine`](#machine-directory),
if there is any.

## Default imports

Default imports are defined for each shell in [`import`](./import/),
and they are usually the the same for most shells, except that the
script syntax differs.

The scripts to import are defined as arrays, for example,
currently in `import/import.bash` we have something like this:

```bash
# Declare files to be loaded/sourced
typeset -a files_to_source;

files_to_source+=(
	"$HOME/.os",
	"${SHELL_CONF}/aliases.sh"
	"${SHELL_CONF}/prompt/prompt.bash"
	"${SHELL_CONF}/paths.sh"
);
```

`files_to_source` here is an array of all files to be sourced sequentially.

The 1st default import should be [`$HOME/.os`](./.os)

> This script detects the OS it's on, and then load OS-specific
> environments for it, e.g. `${SHELL_CONF}/mac` for macOS and
> `${SHELL_CONF}/linux` for GNU/Linux

Other popular imports include:

- [`${SHELL_CONF}/paths.sh`](./paths.sh)

    This file is my PATH modification script

- [`${SHELL_CONF}/aliases.sh`](./aliases.sh)

    Shell command aliases

## Shell prompts

Shell prompt environments are defined in [`prompt`](./prompt/),
and each shell usually has its own prompt setup, due to their differences.

This repository provides `prompt/prompt-git.sh` utility to get
Git information in the PWD to help configuring prompts.

## `.machine` directory

Machine-specific, user-defined environments are defined in `$HOME/.machine`.
They are sourced by the entrypoints last.