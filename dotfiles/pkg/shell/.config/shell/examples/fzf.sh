# FZF plugin
files_to_source=(
	"/usr/share/fzf/completion.bash"
	"/usr/share/fzf/key-bindings.bash"
	"${HOME}/.fzf.bash"
);

for f in ${files_to_source[@]}; do
    . $f;
done;
