#!/usr/bin/env zsh
# Set the terminal title and prompt

autoload -U colors && colors

# autoload -Uz vcs_info
# precmd() {
#     vcs_info
# }
# zstyle ':vcs_info:git:*' formats '%b %u %c'

setopt PROMPT_SUBST

NEWLINE=$'\n'
RESET="%{$reset_color%}"

PS1="${NEWLINE}"
PS1+="%B"
PS1+="%{$fg[white]%}%n%F{37}@%M:%~%f"
PS1+=' %F{61}$(prompt_git)%f${RESET}'; # Git repository details
PS1+="%b"
PS1+="${NEWLINE}zsh %% "
export PS1;

PS2="%[${yellow%}]→ %[${reset%}]";
export PS2;
