# paths.sh - define env $PATH
PROGNAME="paths.sh"

# macOS path can be more easily edited in /etc/paths
# FreeBSD path can be more easily edited at /etc/login.conf, run cap_mkdb /etc/login.conf afterwards

prepend_path_if_exist() {
    target_path="$1";
    old_path="$PATH";

    if [ -d "$target_path" ]; then
        export PATH="$target_path:$PATH"\
            && pront "prepended $target_path to \$PATH";
    fi;
}

prepend_path_if_exist "$HOME/.cargo/bin";
prepend_path_if_exist "$HOME/go/bin";
prepend_path_if_exist "$HOME/bin";

# Prepend CCACHE libraries to $PATH
prepend_path_if_exist "/usr/lib/ccache/bin/";

