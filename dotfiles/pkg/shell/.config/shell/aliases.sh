# .config/shell/aliases.sh
# Shell aliases - tested on bash, ksh (mksh, loksh)

# dmenu replacement on wayland
#[ "$XDG_SESSION_TYPE" = 'wayland' ]\
[ -n "$WAYLAND" ]\
&& alias dmenu='/usr/bin/wofi -d';
alias sway='export WAYLAND=1 && export MOZ_ENABLE_WAYLAND=1 && sway'

# Enable aliases to be sudo/doas’ed
alias sudo='sudo ';
alias doas='doas ';

# Easier navigation: .., ..., ...
alias p='pwd';
alias dl='cd ~/Downloads';
alias dt='cd ~/Desktop';
alias etc='cd /etc';
alias ..='cd ..';
alias ...='cd ../..';
alias ....='cd ../../..';
alias .....='cd ../../../..';

# Shortcuts
alias c='clear';
alias e='exit';
alias t='tmux';
alias ta='tmux attach';
alias td='tmux detach';
alias v='nvim';
alias nv='nvim';
alias vim='nvim';
alias h='hx'
alias hx='helix';
alias g='git';
alias gs='git status';
alias ga='git add';
alias gc='git commit';
alias gp='git push';
alias da='doas';
alias sw='sway';
alias sx='startx';
alias sgdm='sudo systemctl start gdm';
alias hk='less .config/sxhkd/sxhkdrc';
alias www='w3m'
alias tsm='transmission-remote';
alias pingg='ping -c 1 1.1.1.1';

# Reload the shell (i.e. invoke as a login shell)
alias reload='exec ${SHELL} -l';

# Print each PATH entry on a separate line
alias path='echo -e ${PATH//:/\\n}';
alias l='exa';
alias la='ls -lAF';

# Get week number
alias week='date +%V';
