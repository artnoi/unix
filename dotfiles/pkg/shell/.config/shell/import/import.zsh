caller="$1";

# Declare files to be loaded/sourced
typeset -a files_to_source;

# Shell config
files_to_source+=(
	"${SHELL_CONF}/aliases.sh"
	"${SHELL_CONF}/prompt/prompt-git.sh"
	"${SHELL_CONF}/prompt/prompt.zsh"
	"${SHELL_CONF}/paths.sh"
);

for f in ${files_to_source[@]}; do
	source_shell_script "$f" "$caller -> import.zsh";
done;
