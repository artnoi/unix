# These functions will be used down the import ("sourcing") chain.
source_shell_script() {
    local target="$1";
    local caller="$2";

    pront "loading($caller): $target"\

    if [ -d "$target" ]; then
        echo "error:  [$caller]: target is directory - '$target'";
        return 1;

    elif [ ! -r $target ]; then
        echo "error:  [$caller]: target not readable - '$target'";
        return 1;

    elif ! . $target; then
        echo "error:  [$caller]: error from sourcing target '$target'";
        return 1;
    fi

    return 0;
}

source_shell_script_dir() {
    local dir=$1;
    local caller=$2;

    # Only loop through .sh files
    if [ ! -d $dir ]; then
        echo "failed: [$caller]: $dir - dir not directory";

        return

    fi;

    for f in $(find $dir -type f -name "*.*sh"); do
        source_shell_script "$f" "$caller";
    done;
}

