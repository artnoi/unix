caller="$1";

set -A imports\
	"${HOME}/.os"\
	"${SHELL_CONF}/paths.sh"\
	"${SHELL_CONF}/aliases.sh"\
	"${SHELL_CONF}/prompt/prompt.ksh";

for f in "${imports[@]}"; do
	source_shell_script "$f" "$caller -> import.ksh";
done;
