caller="$1";

# Declare files to be loaded/sourced
typeset -a files_to_source;

files_to_source+=(
	"${SHELL_CONF}/aliases.sh"
	"${SHELL_CONF}/prompt/prompt.bash"
	"${SHELL_CONF}/paths.sh"
	"$HOME/.os"
);

for f in ${files_to_source[@]}; do
    source_shell_script "$f" "import.bash";
done;
