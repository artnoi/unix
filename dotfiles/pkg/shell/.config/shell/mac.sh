PROGNAME="mac.sh";

# Returns Homebrew path prefix based on CPU arch.
brew_location() {
    kname="$(uname -a)"

    case "$kname" in

    *"ARM64")
        export MAC_ARCH="aarch64"
        echo "/opt/homebrew"
    ;;

    *"x86_64")
        export MAC_ARCH="amd64"
        echo "/usr/local"
    ;;

    esac;
}


source_shell_script_dir $SHELL_CONF/mac "$PROGNAME";
