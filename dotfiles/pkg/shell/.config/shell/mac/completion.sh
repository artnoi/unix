brew_dir="$(brew_location)";

case "$SHELL" in

*"/bin/bash")
    completion="${brew_dir}/etc/profile.d/bash_completion.sh";
    ;;

*"/bin/zsh")
    completion="${brew_dir}/share/zsh/site-functions"
    ;;

esac

source_shell_script_dir "$completion" "mac/completion.sh"