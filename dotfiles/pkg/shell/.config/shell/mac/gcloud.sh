

gcloud_bash() {
	brew_prefix="$(brew_location)";
	gcloud_path="Caskroom/google-cloud-sdk/latest/google-cloud-sdk";
	gcloud_prefix="${brew_prefix}/${gcloud_path}";

	typeset -a gcloud_files=(
		"path.bash.inc"
		"completion.bash.inc"
	);

	for f in ${gcloud_files[@]};
	do
		file_to_source="${gcloud_prefix}/${f}";
		source_shell_script "$file_to_source" "mac/gcloud.sh"
	done;
}

case "$SHELL" in

*"/bin/bash")
	gcloud_bash
	;;

*"/bin/zsh")
	echo "WARN: shell is zsh, not sourcing bash completion for GCP SDK"

esac