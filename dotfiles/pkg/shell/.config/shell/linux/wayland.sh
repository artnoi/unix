# Wayland support
PROGNAME="wayland.sh"

[ "$XDG_SESSION_TYPE" = 'wayland' ]\
&& export WAYLAND=1\
&& export MOZ_ENABLE_WAYLAND=1\
&& export MOZ_DISABLE_RDD_SANDBOX=1\
&& pront "is Wayland"\
&& return;

pront "not Wayland";
