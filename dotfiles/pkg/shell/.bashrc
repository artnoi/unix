# Reset history
unset HISTFILE;

# From man bash: if interactive,
# (1) PS1 set, (2) $- contains i
if [[ "$-" != *"i"* ]]; then
  # If not running interactively, don't do anything
  return
fi;

# .bashrc defines common, basic Bash envs.
# These settings defined in this file can later be overwritten with
# your own "dynamic" envs.
echo "Starting environment from gitlab.com/artnoi/unix";
export SHELL_CONF="${HOME}/.config/shell";

PROGNAME="bashrc"

# Import sourcing utils
# @TODO: Refactor import.sh as the only entrypoint
. "${SHELL_CONF}/init.sh"
. "${SHELL_CONF}/import/import.bash";

# No persistent shell history
HISTCONTROL=$HISTCONTROL${HISTCONTROL+:}ignoredups;
HISTCONTROL=ignoreboth;

# Window size
shopt -s checkwinsize;

# Load machine specific environment
source_shell_script_dir "$HOME/.machine" ".bashrc";
