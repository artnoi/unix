#!/usr/bin/env bash

for pkgname in *; do
	# Skip README and this file
	test "${pkgname}" == "README.md" || test "${pkgname}" == "diff.sh"\
		&& continue;

    # The directory containing the actual config
    # e.g., if `pkgname` is `alacritty`, then the actual
    # config directory is `./alacritty/.config/alacritty`
    dir="${pkgname}/.config/${pkgname}";

	# User installed path
	dst="~/.config/${pkgname}";

	cmd="diff -r ${dir} ${dst}";
	echo "Showing diff for ${pkgname}: ${cmd}";
	sh -c "${cmd}"\
		&& echo "No changes for ${pkgname}";
done;
