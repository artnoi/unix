" Editor settings
syntax on
filetype plugin indent on
set incsearch
set invnumber
set relativenumber
set smartindent
set tabstop=4 softtabstop=4
set shiftwidth=4
" vim files
set viminfo=

" Enable mouse in all modes if possible
if has('mouse')
	set mouse=a
endif
" Fixes backspace not working
set backspace=indent,eol,start
" Show the cursor position
set ruler
" Show the current mode
set showmode
" Show the filename in the window title bar
set title
" Show the (partial) command as it’s being typed
set showcmd

" Settings for file types
" For .go and .rs files
autocmd BufRead,BufNewFile *.go,*.rs setlocal nowrap
autocmd Filetype html setlocal ts=2 sw=2 expandtab
autocmd Filetype python setlocal ts=2 sw=2 expandtab

" Keybindings
" Paste what u yanked not what deleted
nmap ,p "0p
nmap ,p "0P
" Other keymaps
nmap <up> :resize +5<CR>
nmap <down> :resize -5<CR>
nmap <left> :vertical resize -3<CR>
nmap <right> :vertical resize +3<CR>
nmap <C-f> :NERDTreeToggle<CR>
nmap <C-c> :w !pbcopy<CR>
nmap <C-s> :tabn<CR>
nmap <C-n> :tabedit 
nmap <Leader>l :ALEFix
nmap <Leader>f :FZF<CR>
nnoremap Y y$
nnoremap n nzzzv
nnoremap N Nzzzv
nnoremap J mzJ`z
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-h> <C-w>h
nnoremap <C-l> <C-w>l
nnoremap ; :
vnoremap ; :
nnoremap ' :!
vnoremap ' :!
" Moving text"
inoremap <C-j> <esc>:m .+1<CR>==
inoremap <C-k> <esc>:m .-2<CR>==
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv
nnoremap <Leader>j :m .+1<CR>==
nnoremap <Leader>k :m .-2<CR>==
" Undo break points
inoremap . .<C-g>u
inoremap , ,<C-g>u
inoremap ! !<C-g>u
inoremap ? ?<C-g>u
" LanguageClientNeovim menu
nmap <F5> <Plug>(lcn-menu)
" Or map each action separately
nmap <silent>K <Plug>(lcn-hover)
nmap <silent> gd <Plug>(lcn-definition)
nmap <silent> <F2> <Plug>(lcn-rename)

" Automatically closing braces
""inoremap { {}<Esc>ha
""inoremap ( ()<Esc>ha
""inoremap [ []<Esc>ha
""inoremap " ""<Esc>ha
""inoremap ' ''<Esc>ha
""inoremap ` ``<Esc>ha

" Plugins
call plug#begin()
" Colors
Plug 'cocopon/iceberg.vim'
" vim-airline
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
" Git
Plug 'tpope/vim-fugitive'
" Search
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'airblade/vim-rooter'
Plug 'preservim/NERDTree'
" Go
Plug 'fatih/vim-go', { 'do': ':GoInstallBinaries' }
" Language server clients and linting
Plug 'dense-analysis/ale'
Plug 'autozimu/LanguageClient-neovim', {
\ 'branch': 'next',
\ 'do': 'bash install.sh',
\ }

call plug#end()

colorscheme iceberg
let g:airline_theme='ayu_dark'
let g:airline#extensions#branch#enabled = 1
let g:airline#extensions#ale#enabled = 1
" ALE autocomplete
let g:ale_completion_enabled = 1
" vim-go configuration
let g:go_auto_sameids = 1
" Langauge servers
let g:LanguageClient_serverCommands =
\ {
\	'go': ['gopls'],
\	'rust': ['rust-analyzer'],
\ }
" Fixers, i.e. prettifying formatter
let g:ale_fixers = {
\   'javascript': ['eslint'],
\	'go': ['gofmt'],
\	'rust': ['rustfmt'],
\   'sh': ['shfmt'],
\   '*': ['remove_trailing_lines', 'trim_whitespace'],
\}

" Search
" FZF stuff
let $FZF_DEFAULT_OPTS = '--layout=reverse --info=inline'
let $FZF_DEFAULT_COMMAND="rg --files --hidden"
let g:fzf_tags_command = 'ctags -R'

" Border color
let g:fzf_layout = {'up':'~90%', 'window': { 'width': 0.8, 'height': 0.8,'yoffset':0.5,'xoffset': 0.5, 'highlight': 'Todo', 'border': 'sharp' } }

" Customize fzf colors to match your color scheme
let g:fzf_colors =
\ {
	\ 'fg':      ['fg', 'Normal'],
	\ 'bg':      ['bg', 'Normal'],
	\ 'hl':      ['fg', 'Comment'],
	\ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
	\ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
	\ 'hl+':     ['fg', 'Statement'],
	\ 'info':    ['fg', 'PreProc'],
	\ 'border':  ['fg', 'Ignore'],
	\ 'prompt':  ['fg', 'Conditional'],
	\ 'pointer': ['fg', 'Exception'],
	\ 'marker':  ['fg', 'Keyword'],
	\ 'spinner': ['fg', 'Label'],
	\ 'header':  ['fg', 'Comment']
\ }

" This is the default extra key bindings
let g:fzf_action =
\ {
	\ 'ctrl-t': 'tab split',
	\ 'ctrl-x': 'split',
	\ 'ctrl-v': 'vsplit'
\ }

" Enable per-command history.
" CTRL-N and CTRL-P will be automatically bound to next-history and
" previous-history instead of down and up. If you don't like the change,
" explicitly bind the keys to down and up in your $FZF_DEFAULT_OPTS.
let g:fzf_history_dir = '~/.local/share/fzf-history'

"Get Files
command! -bang -nargs=? -complete=dir Files
	\ call fzf#vim#files(<q-args>, fzf#vim#with_preview({'options': ['--layout=reverse', '--info=inline']}), <bang>0)

" Get text in files with Rg
command! -bang -nargs=* Rg
	\ call fzf#vim#grep(
	\   'rg --column --line-number --no-heading --color=always --smart-case '.shellescape(<q-args>), 1,
	\   fzf#vim#with_preview(), <bang>0)

" Ripgrep advanced
function! RipgrepFzf(query, fullscreen)
	let command_fmt = 'rg --column --line-number --no-heading --color=always --smart-case %s || true'
	let initial_command = printf(command_fmt, shellescape(a:query))
	let reload_command = printf(command_fmt, '{q}')
	let spec = {'options': ['--phony', '--query', a:query, '--bind', 'change:reload:'.reload_command]}
	call fzf#vim#grep(initial_command, 1, fzf#vim#with_preview(spec), a:fullscreen)
endfunction

command! -nargs=* -bang RG call RipgrepFzf(<q-args>, <bang>0)

" Git grep
command! -bang -nargs=* GGrep
	\ call fzf#vim#grep(
	\   'git grep --line-number '.shellescape(<q-args>), 0,
	\   fzf#vim#with_preview({'dir': systemlist('git rev-parse --show-toplevel')[0]}), <bang>0)

" Keybindings
nnoremap <leader>f :Files<CR>
nnoremap <leader>g :Rg<CR>
nnoremap <leader>t :Tags<CR>
nnoremap <leader>m :Marks<CR>
