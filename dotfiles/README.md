# dotfiles

This directory is organized into 2 main types of subdirectories:

1. Shared home configurations in [`pkg`](./pkg/)

    The subdirectories *in `pkg`* is a GNU Stow package -
    **it mirrors standard UNIX-like user home directories**. It includes the
    configurations I use across *all* platforms.
    **`pkg` subdirectories can be `stow`ed directly to home**.

    For example, to install `pkg/alacritty`, do:

    ```shell
    cd pkg;
    stow -t $HOME alacritty;
    ```

2. Home configurations for different platforms.

    These directories' names map to the platforms' names.
    For example, `linux` contains configuration files I'd only use on a
    Linux distro, while `freebsd` contains the dotfiles specifically
    for my FreeBSD setup. These platform directories mirror the root directories
    of their respective platforms.

    For example, to install home dotfiles for Mac, do:

    ```shell
    stow -t $HOME mac
    ```

There's also a script [`diff.sh`](./pkg/diff.sh) for viewing diffs in case
you don't use `stow` (i.e. if you copied the files manually)
