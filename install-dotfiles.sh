#!/usr/bin/env bash
# bash is used because we use `pushd` and `popd` here

. ./install-init.sh;

PROGNAME="install-dotfiles.sh";

command -v stow || die "stow not found in PATH";

# Add user-specific shell config.
mkdir -p $HOME/.machine/{mac,linux};

pushd "dotfiles/pkg" > /dev/null 2>&1;
for d in *; do
    if [ -d $d ]; then
        printf "\n";
        # Dry-run to see if it's safe to stow,
        # with the stow output suppressed
        if stow -n -t "$HOME" "$d" > /dev/null 2>&1; then
            pront "$d is good to stow";

            simyn "Stow ${d}?"\
                && stow -t $HOME "$d"\
                && pront "$d stowed"\
                && continue;
        fi;
        pront "$d not stowed or failed to be stowed";
    fi;
done;
popd;
