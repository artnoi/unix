#!/usr/bin/env bash
# bash is used because we use `pushd` and `popd` here

. ./install-init.sh;

PROGNAME="install-dotfiles-platform.sh";

command -v stow || die "stow in found in PATH";

PLATFORM=$1;

pushd dotfiles > /dev/null 2>&1;
test -d $PLATFORM\
    || die "Directory $PLATFORM does not exists";

# Dry-run to see if it's safe to stow,
# with the stow output suppressed
stow -n -t $HOME "$PLATFORM" > /dev/null 2>&1\
    && simyn "Stow ${PLATFORM}?"\
    && stow -t "$HOME" "$PLATFORM"\
    && pront "$PLATFORM stowed"\
    && popd\
    && exit 0;

popd;

die "$PLATFORM not stowed or failed to be stowed";
