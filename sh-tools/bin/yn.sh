#!/bin/sh
# yn.sh stupid yes/no prompt to be used by POSIX shells
# $answer was neither yes/no
notyn() {
	unset answer
	printf "Please enter Y or N for yes or no\n"
	unset yesno
}

# Simple yes/no prompt
simyn() {
	unset answer
	prompt_msg="$1"

	while true ; do
		printf "$prompt_msg (y/N): "
		read answer

		case "$answer" in 
			Y|y)
				return 0 && break ;;
			N|n)
				return 1 && break ;;	
			*)
				notyn ;;
		esac
	done
}

# Like simyn, but empty string defaults to "yes"
simyn_default_yes() {
	unset answer
	prompt_msg="$1"

	while true ; do
		printf "${prompt_msg} (y/N): "
		read answer

		case "$answer" in 
			Y|y|"")
				return 0 && break ;;
			N|n)
				return 1 && break ;;	
			*)
				notyn ;;
		esac
	done
}

# Simple string prompt
readprompt() {
	unset answer
	printf "%s: " "$1"
	read answer
}
