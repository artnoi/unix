# sysconfig

This directory contains platform-specific system configurations. Each subdirectories mirror the platforms they are named after, for example, `mac` mirrors macOS root filesystem layout, and `linux` mirrors the layout of most GNU/Linux distos, while [`archlinux`](./archlinux/) mirrors the root directory of an Arch system.

There is no install scripts for these system files, you must manually copy them.
