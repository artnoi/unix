set -o pipefail;

# Source simyn.sh, lb.sh
imports=(
	"sh-tools/bin/yn.sh"
	"sh-tools/bin/lb.sh"
);

for f in "${imports[@]}";
do
	. "${f}" || printf "Failed to source %s\n" "${f}";
done;

pront() {
    echo "[$PROGNAME] $@";
}

die() {
    pront "$@"
    exit 1;
}
