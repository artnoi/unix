#!/usr/bin/env node

const exec = require('child_process').exec;

if (!process.argv[2]) {
    console.log("Usage: rd.js [-m, -g] <size> [name]")
    console.log("-m MB, or -g GB")
    process.exit(1)
};

const sw = process.argv[2];
const targetSize = process.argv[3] || "4";
const rdName = process.argv[4] || 'RAM disk';
let size;

if (sw.toUpperCase().includes("G")) {
    size = targetSize * 2097152
} else if (sw.toUpperCase().includes("M")) {
    size = targetSize * 2048;
};

const cmd = "diskutil erasevolume HFS+ '" + rdName + "' `hdiutil attach -nomount ram://" + size + '`';
console.log(`Running: ${cmd}`);
exec(cmd, (error, stdout, stderr) => {
    if (error) {
        console.error(`exec error: ${error}`);
        console.error(`stderr: ${stderr}`)
        return;
    };
    console.log(`stdout: ${stdout}`);
    }
);
