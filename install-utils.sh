#!/usr/bin/env bash

. ./install-init.sh;

PROGNAME="install-utils.sh";

# pull() executes 'git pull' in a given directory
function pull() {
	git -C "$1" pull;
};

# install_go_utils() clones/pulls a Go repository,
# builds the executable(s), and installs it to $HOME/bin
# usage:
# install_go_utils "${URL}" "${name}"
function install_go_util() {
	pushd "${src_dir}";

	[ ! -d "$2" ]\
	&& git clone "$1"\
	|| pull "$2"

	# @TODO: separate build function
	# with a associative array containing
	# build target src (ie. not hard-coded to cmd/)
	simyn "[${PROGNAME}] Build $2"\
	&& pushd "$2"\
	&& go build -o "$2" "./cmd"\
	&& popd;
	
	simyn "[${PROGNAME}] Install $2"\
	&& pushd "$2"\
	&& cp -a "$2" "${HOME}/bin/."\
	&& popd;
	
	popd;
};

function install_go_utils() {
	# go programs
	typeset -A goutils=(
		["gfc"]="https://github.com/artnoi43/gfc.git"
		["csc"]="https://github.com/artnoi43/csc.git"
		["cases"]="https://github.com/artnoi43/cases.git"
	);

	# util is the map key
	for util in "${!goutils[@]}";
	do
		install_go_util "${goutils[${util}]}" "${util}";
		printf "[%s] Installed %s from %s\n" "${PROGNAME}" "${util}" "${goutils[${util}]}";
	done;
};

main() {
	install_go_utils;
}

main;
