#!/usr/bin/env bash

# TODO: Decide if I'll use stow with this too,
# since sh-tools files on the machine are almost always
# 100% identical on the machines and on the repo.

. ./install-init.sh;

PROGNAME="install-sh-tools.sh";
dir=$(pwd);

# Use rsync(1) if available, else use cp(1)
[ -n "$(command -v rsync)" ]\
&& copycmd=rsync\
|| copycmd=cp;

# rsync flag options
rsync_opt='-aP';
# user option string is prefixed with -
if [[ "${1:0:1}" == "-" ]];
then
	[[ $1 == *"v"* ]]\
	&&  rsync_opt="${rsync_opt}v"; # verbose
	[[ $1 == *"n"* ]]\
	&&  rsync_opt="${rsync_opt}n"; # dry-run
fi;

copy_files() {
	# usage: copy_files $src $dest
	# files/dirs to skip
    src="$1";
    dst="$2";

    # Add any files to skip to this array here.
	skip=(

	);

	for s in "${skip[@]}";
	do
		[ "$1" == "${s}" ]\
		&& printf "skipping %s\n" "${s}"\
		&& return;
	done;

	case "${copycmd}" in
		'rsync')
			rsync "${rsync_opt}" "$src" "$dst";
			;;
		'cp')
			cp -a "$src" "$dst";
			;;
	esac;
	
	[[ -n "${rsync_opt}" ]]\
	&& printf "Copied %s to %s using %s\n" "$src" "$dst" "${copycmd}";
};

# Create neccessary directory
mkdir -p "${HOME}/bin";

# Copy sh-tools/bin using rsync(1)
simyn "[${PROGNAME}] Install sh-tools to ${HOME}/bin"\
&& copy_files "./sh-tools/bin/" "${HOME}/bin/";

# Copy binutils
simyn "[${PROGNAME}] Install bin utils to ${HOME}/bin"\
&& copy_files "./utils/bin" "${HOME}/bin/";

# Prompt for utils install
simyn "[${PROGNAME}] Install src utils"\
&& ./install-utils.sh;
