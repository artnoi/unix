# My UNIX-like environment - [`unix`](https://gitlab.com/artnoi/unix)

This repository is my personal configuration dump. There're 3 main categories here:

1. [Shitty shell scripts](./sh-tools/)

    Frequently used shell scripts. Examples are package manager
    wrapper [`up`](./sh-tools/bin/up) and other `dmenu*.sh` (dmenu wrappers).

    To install sh-tools, run `install-sh-tools.sh`:

2. [Dotfiles](./dotfiles/)

    My dotfiles arranged to be easily `stow`ed, isolated, and extended.

    To install dotfiles, cd into `./dotfiles` and choose what to install
    manually by using `stow` or using the provided scripts.

    `install-dotfiles.bash` installs generic dotfiles from `dotfiles/pkg`,
    while `install-dotfiles-platform.sh <TARGET_PLATFORM>` installs packages
    from `dotfiles/<TARGET_PLATFORM>`, i.e. `install-dotfiles-platform freebsd`
    installs files from `dotfiles/freebsd`.

3. [System configuration](./sysconfig/)

    Some system configurations safe to distribute here.

    You should be manually copying files here

Moreover, there're utilities from other repositories that are usually used together
with the scripts/config in this repository.

To install these external utils, run `install-utils.sh`.
